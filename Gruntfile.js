module.exports = function(grunt){
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 3,
                    progressive: true
                },
                files: [{
                    expand: true,
                    cwd: 'src/img',
                    src: '**/*.{png,jpg,gif}',
                    dest: 'dist/img'
                }]
            }
        }
    });

    grunt.registerTask('default', ['imagemin']);
};